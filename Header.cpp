#include "Header.h"
#include <iostream>

void fct6(int a)
{
	if (a < 0 && a > 20)
		std::cout << "KO" << std::endl;
	else
		std::cout << "OK" << std::endl;
}

void fooit3(int d, int f)
{
	while (d <= f)
	{
		std::cout << ++d << ' ';
	}
}
